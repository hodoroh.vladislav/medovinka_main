<?php


namespace Src\App\Table;


use Src\App\App;
use Src\Core\Table\Table;

class ProductTable extends Table
{
    private function productTableprocessing($products){
        $tbody = '';
//data-lazy-load-image="/Images/image-name.png" data-image-classname="pop-in"
        //
        foreach ($products as $value){
            if ($value->available){
                $tbody .=
                    '<tr class="d-flex">
                    <td class="col-sm-2 " >
                        <div class="item" style="height: 100%">
                            <a class="lightbox" href="'.App::getInstance()->getTable('image')->getImage('productimg/'.$value->img).'" class="lightbox">
                                <img async=on decoding="async" class="img-fluid image scale-on-hover" style="width:100%;"  src="'.App::getInstance()->getTable('image')->getImage('productimg/'.$value->img).'" alt="">
                            </a>
                        </div>
                    </td>
                    <td class="col-sm-2">'.$value->name.'</td>
                    <td class="col-sm-6">'.$value->description.'</td>
                    <td class=" col-sm-2">'.$value->price.'</td>               
                </tr>';
            }
        }

        return $tbody;
    }


    public function getProductTable(){
        $products = $this->query("
        SELECT * 
        FROM merchandise
		LEFT JOIN product_category ON merchandise.product_id = product_category.product_id
        where product != 'пчеловод'");
        return $this->productTableprocessing($products);
    }

    public function getBeekiperProductTable(){
        $products = $this->query("
        SELECT * 
        FROM merchandise
		LEFT JOIN product_category ON merchandise.product_id = product_category.product_id
        where product = 'пчеловод'
        
");
//        var_dump($products);
        return $this->productTableprocessing($products);
    }

}




//<div class="col-md-6 col-lg-4 item">
/*                <a class="lightbox" href="<?=App::getInstance()->getTable('image')->getImage('image5.jpg');?>">*/
/*                    <img class="img-fluid image scale-on-hover" src="<?=App::getInstance()->getTable('image')->getImage('image5.jpg');?>">*/
//                </a>
//            </div>