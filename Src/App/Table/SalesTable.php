<?php


namespace Src\App\Table;


use Src\App\App;
use Src\Core\Table\Table;

class SalesTable extends Table
{
    protected $table = 'sales';
    protected $db;

    public function find($id)
    {
        return $this->query('
        SELECT sales.id, sales.description, sales.quantity, sales.date, sales.price, sales.product, customers.name as customer
        FROM sales
        LEFT JOIN customers ON description = customers.customer_id
        WHERE sales.id = ?
        ',[$id],true);
    }

    public function findWithName($id){
        return $this->query('
        SELECT sales.id, sales.description, sales.quantity, sales.date, sales.price, merchandise.name as product
        FROM sales
		LEFT JOIN merchandise ON merchandise_id = sales.product
        WHERE sales.id = ?
        ',[$id],true);
    }

    public function allTable(){
        return $this->query("
        SELECT sales.id, sales.description,sales.product, sales.quantity, sales.date, sales.price, merchandise.name as product
        FROM sales
		LEFT JOIN merchandise ON sales.product = merchandise.merchandise_id

        ");
    }
}