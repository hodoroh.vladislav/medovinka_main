<?php
namespace Src\App\Table;

use Src\App\App;
use Src\Core\Table\Table;
class ImageTable extends Table {

    private static $ip;
    public function getAllImages(){
        return $this->query("select * from gallery where available = 1 order by img_order asc");
    }

    public function getGallery(){
        $gallery_array = [];
        foreach ($this->getAllImages() as $image){
            $gallery_array[] = $image->img;
        }
        return $gallery_array;
    }
    public function getImage($image,$dir = ''){
        if (self::$ip === null) {
            self::$ip = App::getInstance()->getIp();
        }

        return 'http://'.self::$ip.'/medovinka_fremote/img/'.$dir.'/'.$image;
    }
}