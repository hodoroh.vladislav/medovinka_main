<?php

namespace  Src\Core\Html;

class Form
{
    /**
     * @var array
     */
    private $data;
    /**
     * @var string
     */
    public $surround = 'p';

    /**
     * Form constructor.
     * @param array $data
     */
    public function __construct($data =  array())
    {
        $this->data = $data;
    }

    /**
     * @param $html
     * @return string
     */
    protected function surround($html)
    {
        return "<{$this->surround}>{$html}</{$this->surround}>";
    }

    /**
     * @param $index
     * @return mixed|null
     */
    protected function getValue($index)
    {
        if (is_object($this->data)) {
            return $this->data->$index;
        }
        return isset($this->data[$index])? $this->data[$index]: null;
    }

    /**
     * @param $name
     * @param $label
     * @param array $options
     * @return string
     */
    public function input($name, $label, $options = array())
    {
        $type = isset($options['type'])? $options['type']: 'text';

        return $this->surround(
            '<label  >'.$name. '</label><input type="'.$type.'" name="'.$name.'" class="form-control" ">'
        );
    }

    /**
     * @return string
     */
    public function submit()
    {
        return $this->surround('<button type="submit" class="btn btn-secondary">Envoyer</button>');
    }

}