<?php


namespace Src\Core\Html;
use Src\App\App;

class HTML
{
    private static $activeElement;
    private static $tableColumns;


    /**
     * @param array $array
     * [
     *      [ @Path => @String path, @Array $Scripts]
     *      [@Other=> @Array $scripts]
     * ]
     *
     */
    public function getCSS($array = []){
        $scripts = '';
//        var_dump($array);
        foreach ($array as $value){
            if ($value['path'] !== 'other'){
                foreach ($value[0] as $script){
                    $scripts .='<link rel="stylesheet" href="'.$value['path'].'/'.$script.'">';
                }
            }else{
                foreach ($value[0] as $script){
                        $scripts .='<link rel="stylesheet" href="'.$script.'">';
                }
            }
        }
//        var_dump($scripts);
        return $scripts;

    }

    public static function getScripts($page,$options = []){
        $page_arr = [];
        preg_match_all('/[a-zA-Z_].[a-zA-Z-_]*/', $page, $page_arr);
        $proc = current($page_arr);


        if ($proc !== false)
        switch ($proc[0]) {
            case 'index':
                return self::fgScript([
                    'chart' => ['chart-area-demo.js'],
                    'tables'=>['datatables-demo.js']
//                    'jq' => 'https://code.jquery.com/jquery-3.3.1.min.js',
//                    'poopper' => 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
//                    'bootstrap' => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'
                ]);
                break;
            case 'chart':
                return self::fgScript([
                    'charts' => ['chart-area-demo.js','chart-bar-demo.js','chart-pie-demo.js','stockChart.js'],
                ]);
                break;
            case 'tables':
                return self::fgScript([
                    'tables'=>['datatables-demo.js'],
                ]);
                break;
            case 'merchandise':
                return self::fgScript([
                    'tables'=>['datatables-demo.js'],
                ]);
                break;

            case 'other':
                return self::fgScript(['other' => $options]);
                break;
            default:
//                return self::fgScript([
//                    'tables'=>['datatables-demo.js'],
//                ]);

        }
    }

    private static function fgScript($config){
        $scripts = '';


        foreach ($config as $key => $value){
            if ($key === 'other'){
                if (is_array($value))
                    foreach ($value as $secondValue) {
                        if (file_exists('js/'.$secondValue))
                            $scripts .= '<script src="js/'.$secondValue.'"></script>';
                    }
                else
                    $scripts .='<script src="js/'.$value.'"></script>';
            }
            else
            {
                if (is_array($value))
                    foreach ($value as $secondValue) {
                        if (file_exists('js/demo/'.$secondValue))
                            $scripts .= '<script src="js/demo/'.$secondValue.'"></script>';
                    }
                else
                    $scripts .='<script src="js/demo/'.$value.'"></script>';
            }
        }
        return $scripts;
    }

    public static function menu($menuItems){
        $menu = '';
        $menuLink = '';
        foreach ($menuItems as $key => $item) {
            $menuLink
                .= '<li class="nav-item ">
                <a class="nav-link" href="?page='.$key.'">' . $item . ' <span class="sr-only">(current)</span></a>
            </li>';
        }
        $menu =
            '<nav class="navbar navbar-expand-lg navbar-light ">
    <a class="navbar-brand" href="?page=main">Medovinka - Ваша насолода</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto  navli">
            '.$menuLink.'
        </ul>

    </div>
    </nav>';

            return $menu;

    }

    public static function surround($tag, $html,$method = 1)
    {
        if ($method === 1) {
            return str_replace('{HTML}', $html, $tag);
        }
        else if ($method === 2) {
            return '<' . $tag . '>' . $html . '</' . $tag . '>';
        }
    }



}