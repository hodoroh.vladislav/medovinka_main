<?php
/**
 * Created by PhpStorm.
 * User: vladv
 * Date: 12/08/2018
 * Time: 17:49
 */

namespace Src\Core\Statistics;


use Src\App\App;

class Stats
{
    public static $_instance;
    private $db;

    public function __construct()
    {
        $this->db = App::getInstance()->getDb();
        $this->date = date("Y-m-d");
    }

    public static function getInstance(){
        if (is_null(self::$_instance))
            self::$_instance = new Stats();
        return self::$_instance;
    }

    public function getViews(){
        return $this->db->query("select `views`, `hosts` from visits where date = '$this->date'");
    }

    public function run(){
        $visitor_ip = $_SERVER['REMOTE_ADDR'];

        if (count($this->db->query("SELECT `visit_id` FROM `visits` WHERE `date`='$this->date'")) === 0)
        {

            $this->db->execute('delete from ips');
            $this->db->execute("insert into ips set `ip_addr` = '$visitor_ip'");
            $this->db->execute("insert into visits set `date` = '$this->date', `hosts` = 1, `views` = 1");
        }
        else
        {
            $current_ip = $this->db->execute("select `ip_id` from ips where `ip_addr` = '$visitor_ip'");
            if ($current_ip)
            {
                $this->db->execute("update visits set `views` = `views`+1 where `date` = '$this->date'");
            }
            else
            {
                $this->db->execute("insert into ips set ip_addr = '$visitor_ip'");
                $this->db->execute("update visits set `hosts`=`hosts`+1, `views`=`views`+1 where `date` = '$this->date'");
            }
        }
    }
}