<?php


namespace Src\Core\Auth;


use Src\Core\Database\Database;

class DBAuth
{
    private $db;
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function getUserId()
    {
        if ($this->logged())
            return $_SESSION['auth'];
        else
            return false;
    }

    public function login($username,$password)
    {
        $user = $this->db->prepare('SELECT * FROM users WHERE username = ?',[$username],null,true);
        if ($user) {
            if ($user->password === sha1($password)) {
                $_SESSION['auth'] = $user->id;
                return true;
            }
        }
    }
    public function logout($session){
        session_destroy();
        header('Location: admin.php?page=home');

    }
    public function logged()
    {
        return isset($_SESSION['auth']);
    }





}