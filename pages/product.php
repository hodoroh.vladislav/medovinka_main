<?php

use Src\Core\Html\HTML;
use Src\App\App;

$style =  HTML::getCSS([
    ['path' => 'css',['style.css','navbar.css','grid-gallery.css','parallax.css','navbrandsize.css','products.css']],
    ['path'=> 'other',['https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css']]
]);



?>

<style>
    .parallax
    {
        min-height: 45%;

        background-image: url(<?= App::getInstance()->getTable('image')->getImage('/honeybox.jpg')?>);
    }

</style>

<div class="parallax" >
    <div class="parallaxText" id="ptext">
      <span class="parallaxBorder">
        <span class="textSpace">Medovinka</span>
        <span class="textSpace"></span>
      </span>
    </div>
</div>

<div class="galleryIntro">
    <section class="section intro">
        <h2>Lorem ipsum dolor sit amet.</h2>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae quis quo laudantium, maiores a omnis veritatis iste eaque officia tempore illum explicabo totam corporis nisi quasi, unde voluptas, velit, nulla ratione ab! Similique, commodi. Blanditiis, nemo doloremque porro alias, repellendus placeat fuga fugiat. Esse, cumque, id! Ex esse quam recusandae maiores voluptatem libero eaque impedit neque expedita quo molestias enim alias debitis, laboriosam doloremque, id quos voluptas totam pariatur architecto nam! Odio beatae nostrum quo nesciunt, fugiat, dolor saepe repudiandae, ratione excepturi atque quas doloribus vero in. Adipisci nobis labore omnis eligendi natus expedita id. Dolores nobis, quas sit tenetur.
        </p>
    </section>
</div>


<table class="table">

    <thead>
    <tr class="d-flex">
        <th class="col-sm-2"></th>
        <th class="col-sm-2">Продукт</th>
        <th class="col-sm-6">Описание</th>
        <th class="col-sm-2">Цена</th>
    </tr>
    </thead>
    <tbody class="gtable">
        <?php echo  App::getInstance()->getTable('product')->getProductTable();?>
    </tbody>

</table>


<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.gtable', { animation: 'slideIn'});
</script>

