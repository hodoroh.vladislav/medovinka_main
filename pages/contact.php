<?php

use Src\Core\Html\HTML;
use Src\App\App;

$style =  HTML::getCSS([
    ['path' => 'css',['style.css','navbar.css','contact.css','parallax.css','navbrandsize.css']],
    ['path' => 'other',['https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css']]
]);

?>


<style>
    .parallax
    {

        background-image: url(<?=App::getInstance()->getTable('image')->getImage('contactimg.jpeg')?>);
    }

</style>


<div class="parallax" >
    <div class="parallaxText" id="ptext">
        <span class="parallaxBorder">
          <span class="textSpace">Medovinka</span>
          <span class="textSpace"></span>
        </span>
    </div>
</div>



<div class="container mt-5 mb-3">
    <section class="section">
        <h2>About honey</h2>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum atque tempora a et non corporis reprehenderit ipsa cum minima quas voluptatum totam deleniti in illo, ex, animi veniam modi quisquam error. Earum iure dicta repellendus enim voluptates. Odio atque iure delectus, incidunt ex ducimus ullam tempora eligendi earum ab nam dolores, culpa consequuntur laborum sint officiis saepe aperiam perspiciatis officia temporibus! Dolore omnis maxime odit quibusdam. Fuga quibusdam illo, ex minima, voluptates suscipit obcaecati natus voluptas odit saepe quasi aspernatur exercitationem harum labore at animi, accusamus architecto ipsum eaque dolor nostrum adipisci ipsa veniam temporibus. Ea delectus dolore fugit molestiae.
        </p>
    </section >
</div>


<div class="container mb-5">




    <div class="box">
        <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
        <div class='details'><h3>Ukraine, Chernivtsi</h3></div>
    </div>

    <div class="box">
        <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
        <div class='details'><h3>+132456789</h3></div>
    </div>

    <div class="box">
        <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
        <div class='details'><h3>info@gmail.com</h3></div>
    </div>
</div>
