<?php
use Src\App\App;
use Src\Core\Html\HTML;
$style =  HTML::getCSS([
    ['path' => 'css',['style.css','navbar.css','gallery.css','grid-gallery.css','parallax.css','navbrandsize.css']],
    ['path'=> 'other',['https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css']]
    ]);
?>


<style>
    .parallax
    {
        min-height: 45%;

        background-image: url(<?=App::getInstance()->getTable('image')->getImage('/limg/bees3.jpg')?>);
    }
</style>
<div class="parallax" >
    <div class="parallaxText" id="ptext">
      <span class="parallaxBorder">
        <span class="textSpace">Medovinka</span>
        <span class="textSpace"></span>
      </span>
    </div>
</div>



<div class="galleryIntro" >
    <section class="section intro">
        <h2>Lorem ipsum dolor sit amet.</h2>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae quis quo laudantium, maiores a omnis veritatis iste eaque officia tempore illum explicabo totam corporis nisi quasi, unde voluptas, velit, nulla ratione ab! Similique, commodi. Blanditiis, nemo doloremque porro alias, repellendus placeat fuga fugiat. Esse, cumque, id! Ex esse quam recusandae maiores voluptatem libero eaque impedit neque expedita quo molestias enim alias debitis, laboriosam doloremque, id quos voluptas totam pariatur architecto nam! Odio beatae nostrum quo nesciunt, fugiat, dolor saepe repudiandae, ratione excepturi atque quas doloribus vero in. Adipisci nobis labore omnis eligendi natus expedita id. Dolores nobis, quas sit tenetur.
        </p>
    </section>
</div>


<!---->
<!--<div class="mainGallery">-->
<!---->
<!--    <div class="gallery">-->
<!--      <div><img src="img/limg/beehivegroup2.jpg" alt=""></div>-->
<!--	    <div><img src="img/limg/onesideviwbeehives.jpg" alt=""></div>-->
<!--	    <div><img src="img/limg/pbeehiveimg.jpg" alt=""></div>-->
<!--	    <div><img src="img/limg/workprocess.jpg" alt=""></div>-->
<!--	    <div><img src="img/limg/pworkbeehive2.jpg" alt=""></div>-->
<!--    </div>-->
<!--</div>-->







<section class="gallery-block grid-gallery">
    <div class="container">
        <div class="heading">
            <h2>Галерея</h2>
        </div>
        <div class="row">
            <?php foreach (App::getInstance()->getTable('image')->getGallery() as $value):?>
                <div class="col-md-6 col-lg-4 item">
                    <?php $image = App::getInstance()->getTable('Image')->getImage($value,'gallery');?>
                    <a class="lightbox" href="<?=$image?>">
                        <img class="img-fluid image scale-on-hover" src="<?=$image;?>" alt="Image">
                    </a>
                </div>
            <?php endforeach;?>

        </div>
    </div>
</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js" async></script>
<script>
    baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
</script>
