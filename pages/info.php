<?php

use Src\Core\Html\HTML;
use Src\App\App;

$style =  HTML::getCSS([
    ['path' => 'css',['style.css','navbar.css','about.css','parallax.css','navbrandsize.css']],
    ['path'=> 'other',['https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css']]
]);


?>
<style>
    .parallax
    {
        min-height: 45%;

        background-image: url(<?=App::getInstance()->getTable('image')->getImage('originalBee.jpeg');?>);
    }

</style>



<div class="parallax" >
    <div class="parallaxText" id="ptext">
        <span class="parallaxBorder">
          <span class="textSpace">Medovinka</span>
          <span class="textSpace"></span>
        </span>
    </div>
</div>

<section class="sectionAbout">
    <h2>About us</h2>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum atque tempora a et non corporis reprehenderit ipsa cum minima quas voluptatum totam deleniti in illo, ex, animi veniam modi quisquam error. Earum iure dicta repellendus enim voluptates. Odio atque iure delectus, incidunt ex ducimus ullam tempora eligendi earum ab nam dolores, culpa consequuntur laborum sint officiis saepe aperiam perspiciatis officia temporibus! Dolore omnis maxime odit quibusdam. Fuga quibusdam illo, ex minima, voluptates suscipit obcaecati natus voluptas odit saepe quasi aspernatur exercitationem harum labore at animi, accusamus architecto ipsum eaque dolor nostrum adipisci ipsa veniam temporibus. Ea delectus dolore fugit molestiae.
    </p>
</section >


<table class="table">

    <thead>
    <tr class="d-flex">
        <th class="col-sm-2"></th>
        <th class="col-sm-2">Продукт</th>
        <th class="col-sm-6">Описание</th>
        <th class="col-sm-2">Цена</th>
    </tr>
    </thead>
    <tbody class="gtable">
    <?=  App::getInstance()->getTable('product')->getBeekiperProductTable();?>
    </tbody>

</table>



<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.gtable', { animation: 'slideIn'});
</script>
