<?php

use Src\Core\Html\HTML;
use \Src\App\App;
$style =  HTML::getCSS([
        ['path' => 'css',['style.css','slider.css','navbar.css','parallax.css','navbrandsize.css']],
    ]);


?>

<style>


    .parallax
    {
        min-height: 66%;

        background-image: url(<?= App::getInstance()->getTable('image')->getImage('/main.jpg')?>);
    }



</style>
<div class="parallax">
    <div class="parallaxText" id="ptext">
    <span class="parallaxBorder">
      <span class="textSpace">Medovinka</span>

    </span>
    </div>
</div>

<section class="section">
    <h2>About honey</h2>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum atque tempora a et non corporis reprehenderit ipsa cum minima quas voluptatum totam deleniti in illo, ex, animi veniam modi quisquam error. Earum iure dicta repellendus enim voluptates. Odio atque iure delectus, incidunt ex ducimus ullam tempora eligendi earum ab nam dolores, culpa consequuntur laborum sint officiis saepe aperiam perspiciatis officia temporibus! Dolore omnis maxime odit quibusdam. Fuga quibusdam illo, ex minima, voluptates suscipit obcaecati natus voluptas odit saepe quasi aspernatur exercitationem harum labore at animi, accusamus architecto ipsum eaque dolor nostrum adipisci ipsa veniam temporibus. Ea delectus dolore fugit molestiae.
    </p>
</section >


<div class="container-fluid mt-5">
    <section class=" pb-3 text-center">
        <div class="sl-wrapper">
            <ul class="slider">
                <li><img src="<?=App::getInstance()->getTable('Image')->getImage('slider/blackberry.jpeg')?>"  alt="ss"></li>
                <li><img src="<?=App::getInstance()->getTable('Image')->getImage('slider/cheese.jpeg')?>" alt=""></li>
                <li><img src="<?=App::getInstance()->getTable('Image')->getImage('slider/foodHoney.jpg')?>"  alt=""></li>
                <li><img src="<?=App::getInstance()->getTable('Image')->getImage('slider/wine.jpeg')?>" alt=""></li>
            </ul>
        </div>

    </section>
</div>


<script src="script/js/slider.js"></script>
