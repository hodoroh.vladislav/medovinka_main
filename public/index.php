<?php


use Src\App\App;

define('ROOT', dirname(__DIR__));
require ROOT.'/vendor/autoload.php';
if(isset($_GET['page'])) {
    $page = $_GET['page'];
}
else {
    $page = 'index';
}

if ($page === 'logout'){
    $auth->logout($_SESSION);
}




ob_start();

{
    $req = App::getInstance()->getPath($page);
    if (file_exists($req))
        require $req;
    else
//        var_dump(App::getInstance()->getPath('error.404'));
        require App::getInstance()->getPath('error.404');
}

$content = ob_get_clean();
require ROOT.'/pages/templates/default.php';
?>