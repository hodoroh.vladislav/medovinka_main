<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=absolute, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link rel="shortcut icon" href="img/nLittleBee1.jpg" type="image/jpg">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

	<link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="css/navbar.css">


	<link rel="stylesheet" href="css/about.css">
	<link rel="stylesheet" href="css/parallax.css">
	<link rel="stylesheet" href="css/navbrandsize.css">




	<!-- <link rel="stylesheet" href="css/gallery.css">
    <link rel="stylesheet" href="css/about.css">
    <link rel="stylesheet" href="css/contact.css">
    <link rel="stylesheet" href="css/products.css"> -->

	<title>Medovinka</title>
</head>
<body>

<?php include  'bar.php'?>

<div class="parallax" >
      <div class="parallaxText" id="ptext">
        <span class="parallaxBorder">
          <span class="textSpace">Medovinka</span>
          <span class="textSpace"></span>
        </span>
      </div>
    </div>

<section class="sectionAbout">
  <h2>About us</h2>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum atque tempora a et non corporis reprehenderit ipsa cum minima quas voluptatum totam deleniti in illo, ex, animi veniam modi quisquam error. Earum iure dicta repellendus enim voluptates. Odio atque iure delectus, incidunt ex ducimus ullam tempora eligendi earum ab nam dolores, culpa consequuntur laborum sint officiis saepe aperiam perspiciatis officia temporibus! Dolore omnis maxime odit quibusdam. Fuga quibusdam illo, ex minima, voluptates suscipit obcaecati natus voluptas odit saepe quasi aspernatur exercitationem harum labore at animi, accusamus architecto ipsum eaque dolor nostrum adipisci ipsa veniam temporibus. Ea delectus dolore fugit molestiae.
  </p>
</section >
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

</body>
</html>
