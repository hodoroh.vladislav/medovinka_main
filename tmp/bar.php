<?php


echo  '
   
<nav class="navbar navbar-expand-lg navbar-light ">
    <a class="navbar-brand" href="/">Medovinka - Ваша насолода</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto  navli">

            <li class="nav-item active">
                <a class="nav-link" href="index.php">Головна <span class="sr-only">(current)</span></a>
            </li>
            
            <li class="nav-item active">
                <a class="nav-link" href="product.php">Каталог продукціі <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="about.php">Для бжолярів <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="gallery.php">Галерея <span class="sr-only">(current)</span></a>
            </li> 
             <li class="nav-item active">
                <a class="nav-link" href="contact.php">Контакти <span class="sr-only">(current)</span></a>
            </li>



        </ul>

    </div>
</nav>

';