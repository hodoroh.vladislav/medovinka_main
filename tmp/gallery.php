
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=absolute, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="shortcut icon" href="img/nLittleBee1.jpg" type="image/jpg">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/gallery.css">
	<link rel="stylesheet" href="css/parallax.css">
	<link rel="stylesheet" href="css/navbar.css">
	<link rel="stylesheet" href="css/grid-gallery.css">
	<link rel="stylesheet" href="css/navbrandsize.css">





  <!-- <link rel="stylesheet" href="css/gallery.css">
  <link rel="stylesheet" href="css/about.css">
  <link rel="stylesheet" href="css/contact.css">
  <link rel="stylesheet" href="css/products.css"> -->

  <title>Medovinka</title>
</head>
<body>

<?php require  'bar.php'?>


<div class="parallax" >
    <div class="parallaxText" id="ptext">
      <span class="parallaxBorder">
        <span class="textSpace">Medovinka</span>
        <span class="textSpace"></span>
      </span>
    </div>
</div>



  <div class="galleryIntro" >
    <section class="section intro">
    <h2>Lorem ipsum dolor sit amet.</h2>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae quis quo laudantium, maiores a omnis veritatis iste eaque officia tempore illum explicabo totam corporis nisi quasi, unde voluptas, velit, nulla ratione ab! Similique, commodi. Blanditiis, nemo doloremque porro alias, repellendus placeat fuga fugiat. Esse, cumque, id! Ex esse quam recusandae maiores voluptatem libero eaque impedit neque expedita quo molestias enim alias debitis, laboriosam doloremque, id quos voluptas totam pariatur architecto nam! Odio beatae nostrum quo nesciunt, fugiat, dolor saepe repudiandae, ratione excepturi atque quas doloribus vero in. Adipisci nobis labore omnis eligendi natus expedita id. Dolores nobis, quas sit tenetur.
    </p>
    </section>
  </div>


<!---->
<!--<div class="mainGallery">-->
<!---->
<!--    <div class="gallery">-->
<!--      <div><img src="img/limg/beehivegroup2.jpg" alt=""></div>-->
<!--	    <div><img src="img/limg/onesideviwbeehives.jpg" alt=""></div>-->
<!--	    <div><img src="img/limg/pbeehiveimg.jpg" alt=""></div>-->
<!--	    <div><img src="img/limg/workprocess.jpg" alt=""></div>-->
<!--	    <div><img src="img/limg/pworkbeehive2.jpg" alt=""></div>-->
<!--    </div>-->
<!--</div>-->







<section class="gallery-block grid-gallery">
	<div class="container">
		<div class="heading">
			<h2>Галерея</h2>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-4 item">
				<a class="lightbox" href="img/image1.jpg">
					<img class="img-fluid image scale-on-hover" src="img/image1.jpg">
				</a>
			</div>
			<div class="col-md-6 col-lg-4 item">
				<a class="lightbox" href="img/image2.jpg">
					<img class="img-fluid image scale-on-hover" src="img/image2.jpg">
				</a>
			</div>
			<div class="col-md-6 col-lg-4 item">
				<a class="lightbox" href="img/image3.jpg">
					<img class="img-fluid image scale-on-hover" src="img/image3.jpg">
				</a>
			</div>
			<div class="col-md-6 col-lg-4 item">
				<a class="lightbox" href="img/image4.jpg">
					<img class="img-fluid image scale-on-hover" src="img/image4.jpg">
				</a>
			</div>
			<div class="col-md-6 col-lg-4 item">
				<a class="lightbox" href="img/image5.jpg">
					<img class="img-fluid image scale-on-hover" src="img/image5.jpg">
				</a>
			</div>


		</div>
	</div>
</section>







<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
</script>




</body>
</html>
