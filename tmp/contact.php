
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=absolute, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="img/nLittleBee1.jpg" type="image/jpg">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/parallax.css">
	<link rel="stylesheet" href="css/navbar.css">
	<link rel="stylesheet" href="css/navbrandsize.css">
	<link rel="stylesheet" href="css/parallax.css">

    <link rel="stylesheet" href="css/contact.css">



    <!-- <link rel="stylesheet" href="css/gallery.css">
    <link rel="stylesheet" href="css/about.css">
    <link rel="stylesheet" href="css/contact.css">
    <link rel="stylesheet" href="css/products.css"> -->

    <title>Medovinka</title>
</head>
<body>

<?php  require  'bar.php'?>


<div class="parallax" >
	<div class="parallaxText" id="ptext">
        <span class="parallaxBorder">
          <span class="textSpace">Medovinka</span>
          <span class="textSpace"></span>
        </span>
	</div>
</div>

<div class="container mt-5">
	<div class="row">
		<div class="col col-lg-9 col- col-sm-9">
			<div class="row">
				<div class="col col-lg-12">
					<h1>Contacts</h1>
				</div>
			</div>
			<div class="row">
				<div class="col col-lg-12">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias aspernatur, blanditiis esse eveniet ex incidunt perspiciatis quasi quia quos repellat repellendus sint sunt. Aperiam assumenda consectetur dignissimos ea fuga illum in incidunt ipsa laboriosam minus molestias natus, numquam odit perspiciatis porro quae quam quasi, recusandae sed sequi temporibus veritatis!
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab blanditiis culpa dolor, hic libero nemo nostrum nulla pariatur recusandae tempora. Dolore et hic nemo officiis.
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-sm-3">
			<div class="row">
				<div class="col col-lg-12 text-center">Contact Information</div>
			</div>
			<div class="row">
				<div class="col col-lg-3">Phone<b>:</b></div>
				<div class="col col-lg-9 text-right">4151153138114</div>
			</div>
			<div class="row">
				<div class="col col-lg-3">Mail<b>:</b></div>
				<div class="col col-lg-9 text-right">testmail@gmail.com</div>

			</div>
			<div class="row">
				<div class="col col-lg-4 ">Adresse<b>:</b></div>
				<div class="col col-lg-8  text-right">21th rue Jean-Tomatte</div>
			</div>
			<div class="row">
				<div class="col col-lg-12 col-sm-12 text-right">67tomates</div>
			</div>
			<div class="row">
				<div class="col col-lg-12 col-sm-12 text-right">TomatoCity</div>
			</div>
			<div class="row">
				<div class="col col-lg-12 col-sm-12 text-right">TamatoLand</div>
			</div>
		</div>
	</div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<!--<script src="js/menu.js" type="text/javascript"></script>-->
</body>
</html>
